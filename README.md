# workstation-config

## Ansible

[Ansible User Guide](https://docs.ansible.com/ansible/latest/user_guide/index.html)

```bash
sudo dnf install ansible
sudo ansible-pull -U https://gitlab.com/etherealy/workstation
```

## Hand installed

### chrome
### vscode

```bash
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'

sudo dnf check-update
sudo dnf install code
```

 ### IntelliJ IDEA

```bash
curl https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.17.7139.tar.gz?_ga=2.252522058.2140998222.1594367201-483073611.1594367201
sudo tar -xzf jetbrains-toolbox-1.13.4801.tar.gz -C /opt
```


### oc

dl rh, move to /usr/local/bin


### gitmoji

npm i -g gitmoji-cli


### gpg key, pass, ssh key 